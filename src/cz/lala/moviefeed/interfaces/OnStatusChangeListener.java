package cz.lala.moviefeed.interfaces;


public interface OnStatusChangeListener {
	public static final int STATUS_PARSER_COMPLETE = 0;
	public static final int STATUS_PARSER_ERROR = 1;
	public static final int STATUS_PARSE_NO_MORE_MOVIES = 2;
	public static final int STATUS_PARSER_LOADING_MOVIES = 3;
	public static final int STATUS_DB_COMPLETE = 4;
	public static final int STATUS_DB_ERROR = 5;
	
	public void onStatusChange(int mStatus);
	public void isMovieParserRunning(boolean isFilmParserRunning);
}

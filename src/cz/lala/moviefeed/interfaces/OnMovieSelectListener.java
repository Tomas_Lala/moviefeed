package cz.lala.moviefeed.interfaces;

public interface OnMovieSelectListener {
	public void OnMovieSelect(int position);

}
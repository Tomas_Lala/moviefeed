package cz.lala.moviefeed.parser;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import cz.lala.moviefeed.interfaces.OnStatusChangeListener;
import cz.lala.moviefeed.models.Movie;
import cz.lala.moviefeed.repository.MovieRepository;

public class MovieParser extends AsyncTask<String, Void, Void>{
	

	
	
	private static final String URL_PART_ONE = "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/upcoming.json?page_limit=";
	private static final String URL_PART_TWO = "&page=";
	private static final String URL_PART_THREE = "&country=us&apikey=";
	private static final String APIKEY = "gs2dn6z369pa34pzjmda864e";
	private static final int FIRSTPAGENUMBER = 1;
	
	
	private static final String TAG_MOVIE = "movies";
	private static final String TAG_TOTTAL = "total";
	private static final String TAG_TITLE = "title";
	private static final String TAG_YEAR = "year";
	private static final String TAG_LENGTH = "runtime";
	private static final String TAG_RELEASE = "release_dates";
	private static final String TAG_RELEASE_THEATER = "theater";
	private static final String TAG_RELEASE_DVD = "dvd";
	private static final String TAG_RATING = "ratings";
	private static final String TAG_CRITIC_RATING = "critics_rating";
	private static final String TAG_CRITIC_SCORE = "critics_score";
	private static final String TAG_AUDIENCE_RATING = "audience_rating";
	private static final String TAG_AUDIENCE_SCORE = "audience_score";
	private static final String TAG_DESCRIBE = "synopsis";
	private static final String TAG_POSTER_DETAILED = "detailed";
	private static final String TAG_ABRIDGE_CAST = "abridged_cast";
	private static final String TAG_ABRIDGE_CAST_NAME = "name";
	private static final String TAG_POSTERS = "posters";

	
	private JSONParser mJsonParser;	
	private ArrayList<Movie> mArrayFilms;
	private List<String> mArrayCharacters;
	private OnStatusChangeListener mCallBack;
	private int mStatus = OnStatusChangeListener.STATUS_PARSER_COMPLETE;
	private int mPageNumber;
	private int mMoviesRemaining;
	private int mPageLimit;
	
	
	public MovieParser() {}
	
	public MovieParser(OnStatusChangeListener mCallBack) {
		this.mCallBack = mCallBack;
	}
	@Override
	protected void onPreExecute() {
		mCallBack.isMovieParserRunning(true);
		mArrayFilms = new ArrayList<Movie>();
		mPageNumber = MovieRepository.getPageNumber();
		mJsonParser  = new JSONParser(); 
		mMoviesRemaining = MovieRepository.getMoviesRamaining();
		mPageLimit = MovieRepository.getPageLimit();
		
		// mMoviesRemaining > 0 jsou zde dalsi filmy
		// mMoviesRemaining == 0 parser bezi poprve
		// mMoviesRemaining < 0 vsechny filmy zobrazeny
		if (mMoviesRemaining<0) {
			mStatus = OnStatusChangeListener.STATUS_PARSE_NO_MORE_MOVIES;
			mCallBack.onStatusChange(OnStatusChangeListener.STATUS_PARSE_NO_MORE_MOVIES);
			this.cancel(true);
		}
		else
		{
			mCallBack.onStatusChange(OnStatusChangeListener.STATUS_PARSER_LOADING_MOVIES);
		}
		
		super.onPreExecute();
	}
	
	@Override
	protected Void doInBackground(String... params) {	
	
		try {
			
		JSONObject json = mJsonParser.makeHttpRequest(
                 (URL_PART_ONE+mPageLimit+URL_PART_TWO+mPageNumber+URL_PART_THREE+APIKEY), "GET", null);
		
			if (null!=json.getString(TAG_TOTTAL)) {
				MovieRepository.setPageNumber(mPageNumber+1);
				// Pokud jsem na prvni strance ulozim pocet filmu
				if (mPageNumber==FIRSTPAGENUMBER) {
					MovieRepository.setMoviesRamaining(json.getInt(TAG_TOTTAL));
				}
				
				// Rozdeleni na filmy
				JSONArray movies = json.getJSONArray(TAG_MOVIE); 
				for (int i = 0; i < movies.length(); i++) {
					mArrayCharacters = new ArrayList<String>();
					JSONArray characters = movies.getJSONObject(i).getJSONArray(TAG_ABRIDGE_CAST);
					JSONObject ratings = movies.getJSONObject(i).getJSONObject(TAG_RATING);
					JSONObject release = movies.getJSONObject(i).getJSONObject(TAG_RELEASE);
					
					// Parsovani hercu
					for (int j = 0; j < characters.length(); j++) {
						mArrayCharacters.add(characters.getJSONObject(j).getString(TAG_ABRIDGE_CAST_NAME));
					}
					
				// Pridani noveho filmu do ArrayListu
					mArrayFilms.add(new Movie(movies.getJSONObject(i).getString(TAG_TITLE), 
							release.has(TAG_RELEASE_THEATER) ? release.getString(TAG_RELEASE_THEATER) : null,
							release.has(TAG_RELEASE_DVD) ? release.getString(TAG_RELEASE_DVD) : null,
							movies.getJSONObject(i).getInt(TAG_YEAR),
							ratings.has(TAG_CRITIC_RATING) ? ratings.getString(TAG_CRITIC_RATING) : null,
							ratings.has(TAG_CRITIC_SCORE) ? ratings.getInt(TAG_CRITIC_SCORE) : (-1),
							ratings.has(TAG_AUDIENCE_RATING) ? ratings.getString(TAG_AUDIENCE_RATING) : null,
							ratings.has(TAG_AUDIENCE_SCORE) ? ratings.getInt(TAG_AUDIENCE_SCORE) : (-1),
							movies.getJSONObject(i).getInt(TAG_LENGTH), 
							movies.getJSONObject(i).getString(TAG_DESCRIBE), mArrayCharacters,
							getBitmapFromUrl(movies.getJSONObject(i).getJSONObject(TAG_POSTERS).getString(TAG_POSTER_DETAILED))));	
				 }
				
			}
			else
			{
				mStatus = OnStatusChangeListener.STATUS_PARSER_ERROR;
				mCallBack.isMovieParserRunning(false);
				mCallBack.onStatusChange(OnStatusChangeListener.STATUS_PARSER_ERROR);
				this.cancel(true);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			mStatus = OnStatusChangeListener.STATUS_PARSER_ERROR;
			mCallBack.isMovieParserRunning(false);
			mCallBack.onStatusChange(OnStatusChangeListener.STATUS_PARSER_ERROR);
			this.cancel(true);
		}
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		mCallBack.isMovieParserRunning(false);
		// Pokud je vse Ok ulozi list do repository, pricte stranku, snizi cislo zbyvajicich filmu o pageLimit
		if (mStatus==OnStatusChangeListener.STATUS_PARSER_COMPLETE) {
			MovieRepository.setArrayFilms(mArrayFilms);
			MovieRepository.removeNumberOfDownloadedMovies(mPageLimit);
			mCallBack.onStatusChange(OnStatusChangeListener.STATUS_PARSER_COMPLETE);
		}			
		super.onPostExecute(result);
	}

	// Ziskani Bitmapy(Obrazek Filmu) z URL
	private Bitmap getBitmapFromUrl(String src) {
		 try {
		        URL url = new URL(src);
		        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		        connection.setDoInput(true);
		        connection.connect();
		        InputStream input = connection.getInputStream();
		        Bitmap myBitmap = BitmapFactory.decodeStream(input);
		        return myBitmap;
		    } catch (IOException e) {
		        return null;
		    }
	}

	

}

package cz.lala.moviefeed.database;

import android.provider.BaseColumns;

public abstract class MovieColum implements BaseColumns {

	 public static final String TABLE_NAME = "movies";
     public static final String COLUMN_NAME_ID = "movie_id";
     public static final String COLUMN_NAME_TITLE = "title";
     public static final String COLUMN_NAME_YEAR = "year";
     public static final String COLUMN_NAME_LENGTH = "runtime";
     public static final String COLUMN_NAME_RELEASE_THEATER = "theater";
     public static final String COLUMN_NAME_RELEASE_DVD = "dvd";
     public static final String COLUMN_NAME_CRITIC_RATING = "critics_rating";
     public static final String COLUMN_NAME_CRITIC_SCORE = "critics_score";
     public static final String COLUMN_NAME_AUDIENCE_RATING = "audience_rating";
     public static final String COLUMN_NAME_AUDIENCE_SCORE = "audience_score";
     public static final String COLUMN_NAME_DESCRIBE = "synopsis";
     public static final String COLUMN_NAME_POSTER_DETAILED = "detailed";
     public static final String COLUMN_NAME_ABRIDGE_CAST_CHARACTERS = "characters";
}

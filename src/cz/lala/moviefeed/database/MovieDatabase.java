package cz.lala.moviefeed.database;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import cz.lala.moviefeed.interfaces.OnStatusChangeListener;
import cz.lala.moviefeed.models.Movie;
import cz.lala.moviefeed.repository.MovieRepository;

public class MovieDatabase extends SQLiteOpenHelper{
	

	
	private static final String DATABASE_NAME = "LalasFilmer.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String TEXT_TYPE = " TEXT";
	private static final String INTEGER_TYPE = " INTEGER";
	private static final String BLOB_TYPE = " BLOB";
	private static final String COMMA_SEP = ",";
	
	private static final String SQL_CREATE_ENTRIES =
		    "CREATE TABLE " + MovieColum.TABLE_NAME + " (" +
		    MovieColum.COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
		    MovieColum.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_LENGTH + INTEGER_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_YEAR + INTEGER_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_DESCRIBE + TEXT_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_RELEASE_THEATER + TEXT_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_RELEASE_DVD + TEXT_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_AUDIENCE_RATING + TEXT_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_CRITIC_RATING + TEXT_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_AUDIENCE_SCORE + INTEGER_TYPE + COMMA_SEP +
		    MovieColum.COLUMN_NAME_CRITIC_SCORE + INTEGER_TYPE + COMMA_SEP +
		   
		    MovieColum.COLUMN_NAME_ABRIDGE_CAST_CHARACTERS + TEXT_TYPE + COMMA_SEP +
		    
		    MovieColum.COLUMN_NAME_POSTER_DETAILED + BLOB_TYPE + 
		    " )";

		private static final String SQL_DELETE_TABLE =
		    "DROP TABLE IF EXISTS " + MovieColum.TABLE_NAME;
		
		private static final String SQL_READ_ENTRIES =
				"SELECT * FROM " + MovieColum.TABLE_NAME;
		private static final String SQL_DELETE_MOVIES =
				"DELETE FROM " + MovieColum.TABLE_NAME;
	
	
	public MovieDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ENTRIES);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		 db.execSQL(SQL_DELETE_TABLE);
	     onCreate(db);

	}
	public void storeMoviesToDb()
	{
		SQLiteDatabase db = this.getWritableDatabase();	
		db.execSQL(SQL_DELETE_MOVIES);
		ArrayList<Movie> arrayMovies= MovieRepository.getArrayMovies();
		ByteArrayOutputStream bos = null;
		try {
			for (Movie movie : arrayMovies) {
				String characters = null;
				for(String character : movie.getArrayCharacters())
				{
					characters +=character+";";
				}
				characters = characters.substring(0, characters.length()-1);
				
				bos = new ByteArrayOutputStream();	
				byte[] mCoverArray =null;
				try {
					movie.getCover().compress(Bitmap.CompressFormat.PNG, 100, bos);
					mCoverArray = bos.toByteArray();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				ContentValues values = new ContentValues();
				values.put(MovieColum.COLUMN_NAME_TITLE , movie.getName());
				values.put(MovieColum.COLUMN_NAME_LENGTH, movie.getLength());
				values.put(MovieColum.COLUMN_NAME_RELEASE_THEATER, movie.getDateTheater());
				values.put(MovieColum.COLUMN_NAME_RELEASE_DVD,movie.getDateDvd());
				values.put(MovieColum.COLUMN_NAME_CRITIC_RATING, movie.getCritic_rating());
				values.put(MovieColum.COLUMN_NAME_CRITIC_SCORE, movie.getCritic_score());
				values.put(MovieColum.COLUMN_NAME_AUDIENCE_RATING,movie.getAudience_rating());
				values.put(MovieColum.COLUMN_NAME_AUDIENCE_SCORE,movie.getAudience_score());
				values.put(MovieColum.COLUMN_NAME_DESCRIBE,movie.getDescribe());
				values.put(MovieColum.COLUMN_NAME_POSTER_DETAILED, mCoverArray);
				values.put(MovieColum.COLUMN_NAME_ABRIDGE_CAST_CHARACTERS, characters);
				values.put( MovieColum.COLUMN_NAME_YEAR, movie.getYear());
	
				db.insert(MovieColum.TABLE_NAME, null, values);
				
			}
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		db.close();
		this.close();
	}
	public void readMoviesFromDb(OnStatusChangeListener mCallBack)
	{
		SQLiteDatabase db = this.getReadableDatabase();
		
		ArrayList<Movie> arrayMovies= new ArrayList<Movie>();
		Cursor cursor = db.rawQuery(SQL_READ_ENTRIES, null);	
		try {		
			if(cursor.moveToFirst())
			{
				do {
				arrayMovies.add(new Movie(cursor.getString(1), cursor.getString(5), cursor.getString(6),  cursor.getInt(3), cursor.getString(8), cursor.getInt(10),
						cursor.getString(7), cursor.getInt(9), cursor.getInt(2), cursor.getString(4),
						Arrays.asList(cursor.getString(11).split(";")), BitmapFactory.decodeByteArray(cursor.getBlob(12) , 0, cursor.getBlob(12).length)));
				
			
				} while (cursor.moveToNext());
			MovieRepository.setArrayFilms(arrayMovies);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		cursor.close();
		db.close();
		mCallBack.onStatusChange(OnStatusChangeListener.STATUS_DB_COMPLETE);
	}
	public boolean areMoviesStored() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor mCursor = db.rawQuery(SQL_READ_ENTRIES, null);
		if (mCursor.getCount()!=0) {
			return true;
		}
		
		return false;
	}

}

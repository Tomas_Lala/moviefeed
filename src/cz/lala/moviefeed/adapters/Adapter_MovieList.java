package cz.lala.moviefeed.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cz.lala.moviefeed.R;
import cz.lala.moviefeed.models.Movie;
import cz.lala.moviefeed.repository.MovieRepository;

public class Adapter_MovieList extends BaseAdapter {
	private ArrayList<Movie> arrayFilm;
	private LayoutInflater inflater;
	private Context mContext;
	
	public Adapter_MovieList() {}
	
	public Adapter_MovieList(Context mContext) {
		arrayFilm = MovieRepository.getArrayMovies();
		this.mContext =mContext;
	}

	@Override
	public int getCount() {
		return arrayFilm.size();
	}

	@Override
	public Object getItem(int position) {
		return arrayFilm.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		 View layout = convertView;
	       if (layout == null){
	    	inflater  = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        layout = inflater.inflate(R.layout.fr_movielist_row, parent,false);
	        }
	        TextView mName = (TextView)layout.findViewById(R.id.fr_filmlist_row_textview_name);
	        TextView mScore = (TextView)layout.findViewById(R.id.fr_filmlist_row_textview_score);
	        TextView mTheater = (TextView)layout.findViewById(R.id.fr_filmlist_row_textview_theatre);
	        ImageView mCover = (ImageView)layout.findViewById(R.id.fr_filmlist_row_imageview_cover);
	        
	        mName.setText(arrayFilm.get(position).getName());
	        mCover.setImageBitmap(arrayFilm.get(position).getCover());
	        mScore.setText("Score: " + arrayFilm.get(position).getAudience_score());
	        mTheater.setText("Comming " + arrayFilm.get(position).getDateTheater());
	        return layout;
	}

}

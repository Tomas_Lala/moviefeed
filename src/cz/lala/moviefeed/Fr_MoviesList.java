package cz.lala.moviefeed;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import cz.lala.moviefeed.adapters.Adapter_MovieList;
import cz.lala.moviefeed.database.MovieDatabase;
import cz.lala.moviefeed.interfaces.OnMovieSelectListener;
import cz.lala.moviefeed.interfaces.OnStatusChangeListener;
import cz.lala.moviefeed.parser.MovieParser;
import cz.lala.moviefeed.repository.MovieRepository;

public class Fr_MoviesList extends SherlockListFragment implements OnStatusChangeListener,OnScrollListener,OnItemClickListener{
		
	
	private boolean viewsHaveBeenDestroyed = false;
	private boolean isFilmParserRunning = false;
	private boolean isScrollDownloadingEnable = false;
	private boolean isSyncItemEnable = false;
	
	private Adapter_MovieList mAdapter;
	private MovieDatabase mDatabse;
	private int preLastItem;
	private View mFooterLoading;
	private View mFooterError;
	private View mFooterNoMoreFilms;
	private OnMovieSelectListener mCallback;
	private Handler mUIHandler;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mUIHandler = new Handler();
		mDatabse = new MovieDatabase(getSherlockActivity());
		setRetainInstance(true);
		setHasOptionsMenu(true);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);	
		 
		try {
	            mCallback = (OnMovieSelectListener) activity;
	        } catch (ClassCastException e) {
	            e.printStackTrace();
	        }
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);	
		
		if (null!=MovieRepository.getArrayMovies()) {
			if (null==MovieRepository.getmFooter()&&!isScrollDownloadingEnable) {				
				isSyncItemEnable=true;
				getSherlockActivity().supportInvalidateOptionsMenu();
			}
			showMovies();
			return;
		}
		if (!isFilmParserRunning) {
			if (isConnectedToInternet()) {
				downloadMovies();
				return;
			}
			else if((mDatabse.areMoviesStored())){
				mDatabse.readMoviesFromDb(this);
				isSyncItemEnable=true;
				getSherlockActivity().supportInvalidateOptionsMenu();
				return;
			}
			else
			{
				
			}
		}
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		getSherlockActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(false);

		mFooterLoading =getLayoutInflater(savedInstanceState).inflate(R.layout.fr_movielist_footer_loading, getListView(),false);
		mFooterError= getLayoutInflater(savedInstanceState).inflate(R.layout.fr_movielist_footer_error, getListView(),false);
		mFooterNoMoreFilms = getLayoutInflater(savedInstanceState).inflate(R.layout.fr_movielist_footer_nomorefilms, getListView(),false);
		
		getListView().setOnScrollListener(this);
		getListView().setOnItemClickListener(this);
		
		if (MovieRepository.getmFooter()!=null) {
			getListView().addFooterView(MovieRepository.getmFooter());
		}
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fr_list_movie, menu);
		if (isSyncItemEnable) {
			MenuItem mSyncItem = menu.findItem(R.id.fr_list_movie_sync);
			mSyncItem.setVisible(true);
		}
		super.onCreateOptionsMenu(menu, inflater);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.fr_list_movie_sync:	
			if (isConnectedToInternet()) {
				// App je nactena z Db
				if (MovieRepository.getPageNumber()==0) {
					MovieRepository.removeAllMovies();
					mAdapter.notifyDataSetChanged();
				}
				downloadMovies();
				isSyncItemEnable=false;
				getSherlockActivity().supportInvalidateOptionsMenu();
			}
			else
			{
				 Toast.makeText(getSherlockActivity(), getResources().getString(R.string.toast_connect_to_internet), Toast.LENGTH_SHORT).show();
			}
			
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void downloadMovies() {
		if (!isFilmParserRunning) {
			new MovieParser(this).execute();
		}
		
	}

	// Pokud neni Adapter v ListView tak ho nastavi.
	// Pokud jiz je tak jen obnovi data.
	private void showMovies() {
		if (null==getListAdapter()) {
			mAdapter = new Adapter_MovieList(getSherlockActivity());
			setListAdapter(mAdapter);
		}
		else
		{
			mAdapter.notifyDataSetChanged();
		}
		
	}
	
	public boolean isConnectedToInternet()
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) getSherlockActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		if((activeNetworkInfo != null) && (activeNetworkInfo.isConnected()))
		{
			   return true;
		}
		return false;
	}

	// Scroll Listener
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {}

	// Pokud je vide posledni item v listu zpusti se stahovani
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		 final int lastItemList = firstVisibleItem + visibleItemCount+getListView().getFooterViewsCount();
			if((lastItemList == totalItemCount)) {
            if((preLastItem!=lastItemList)&&isScrollDownloadingEnable){ 
              preLastItem =lastItemList;
              downloadMovies();
            }
		 }
	}
	
	// InterfaceStatusAsync
	@Override
	public void onStatusChange(final int mStatus) {	
            	try {
		switch (mStatus) {
		
		// MovieParser
		case OnStatusChangeListener.STATUS_PARSER_LOADING_MOVIES:
			if (null!=getListAdapter()) {
				isScrollDownloadingEnable=false;
				getListView().addFooterView(mFooterLoading);
				MovieRepository.setmFooter(mFooterLoading);
			}		
			break;
		case OnStatusChangeListener.STATUS_PARSER_COMPLETE:
			isScrollDownloadingEnable=true;
			showMovies();
			getListView().removeFooterView(mFooterLoading);
			MovieRepository.setmFooter(null);
			mDatabse.storeMoviesToDb();
			break;
		case OnStatusChangeListener.STATUS_PARSE_NO_MORE_MOVIES:
			isScrollDownloadingEnable=false;
			getListView().addFooterView(mFooterNoMoreFilms);
			MovieRepository.setmFooter(mFooterNoMoreFilms);
			break;
		case OnStatusChangeListener.STATUS_PARSER_ERROR:
			mUIHandler.post(new Runnable() {			
				@Override
				public void run() {
					isScrollDownloadingEnable=false;
					getListView().removeFooterView(mFooterLoading);
					getListView().addFooterView(mFooterError);
					MovieRepository.setmFooter(mFooterError);
					
				}
			});
			break;
		// MovieDatabase
		case OnStatusChangeListener.STATUS_DB_COMPLETE:	
			showMovies();
			break;
		case OnStatusChangeListener.STATUS_DB_ERROR:
			getListView().addFooterView(mFooterError);
			MovieRepository.setmFooter(mFooterError);
			break;
		}  
        } catch (Exception e) {
			e.printStackTrace();
		}
     
	}

	@Override
	public void isMovieParserRunning(boolean isFilmParserRunning) {
		this.isFilmParserRunning = isFilmParserRunning;
	}
	
	// OnItemListListener
	@Override
	public void onItemClick(AdapterView<?> parent, View mTouchedView, int position,
			long id) {
		switch (mTouchedView.getId()) {
		case R.id.fr_filmlist_footer_laoding: break;
		case R.id.fr_filmlist_footer_nomoremovies: break;
		
		case R.id.fr_filmlist_footer_error:
			if (isConnectedToInternet()) {
				  isScrollDownloadingEnable=true;
				  downloadMovies();
				  getListView().removeFooterView(mTouchedView);
				  }
				  else
				  {
					  Toast.makeText(getSherlockActivity(), getResources().getString(R.string.toast_connect_to_internet), Toast.LENGTH_SHORT).show();
				  }
			break;

		
		default:
			mCallback.OnMovieSelect(position);
			break;
		}
		
	}
	// Stop Anim on Rotate
	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
	    // This stops animation on rotation as we have a retained instance.
	    boolean shouldNotAnimate = enter && viewsHaveBeenDestroyed;
	    viewsHaveBeenDestroyed = false;
	    return shouldNotAnimate ? AnimationUtils.loadAnimation(getActivity(), R.anim.none)
	            : super.onCreateAnimation(transit, enter, nextAnim);
	}

	@Override
	public void onDestroyView() {
	    super.onDestroyView();
	    viewsHaveBeenDestroyed = true;
	}




	




}



package cz.lala.moviefeed.models;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;

public class Movie {
	private String name;
	private String dateTheater;
	private String dateDvd;
	private int year;
	private String critic_rating;
	private int critic_score;
	private String audience_rating;
	private int audience_score;
	private String describe;
	private List<String> arrayCharacters;
	private int length;
	private Bitmap cover;
	
	public Movie(){}
	
	public Movie(String name,String dateTheater, String dateDvd, int year,
			String critic_rating,int critic_score,String audience_rating,int audience_score,
			int length, String destribe, List<String> arrayCharacters, Bitmap cover)
	{
		this.name = name;
		this.dateTheater = dateTheater;
		this.dateDvd = dateDvd;
		this.year = year;
		this.critic_rating = critic_rating;
		this.critic_score = critic_score;
		this.audience_rating = audience_rating;
		this.audience_score = audience_score;
		this.describe = destribe;
		this.arrayCharacters = arrayCharacters;		
		this.length = length;
		this.cover = cover;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDateTheater() {
		return dateTheater;
	}
	public void setDateTheater(String dateTheater) {
		this.dateTheater = dateTheater;
	}
	public String getDateDvd() {
		return dateDvd;
	}
	public void setDateDvd(String dateDvd) {
		this.dateDvd = dateDvd;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public List<String> getArrayCharacters() {
		return arrayCharacters;
	}
	public void setArrayCharacters(ArrayList<String> arrayCharacters) {
		this.arrayCharacters = arrayCharacters;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getCritic_rating() {
		return critic_rating;
	}

	public void setCritic_rating(String critic_rating) {
		this.critic_rating = critic_rating;
	}

	public int getCritic_score() {
		return critic_score;
	}

	public void setCritic_score(int critic_score) {
		this.critic_score = critic_score;
	}

	public String getAudience_rating() {
		return audience_rating;
	}

	public void setAudience_rating(String audience_rating) {
		this.audience_rating = audience_rating;
	}

	public int getAudience_score() {
		return audience_score;
	}

	public void setAudience_score(int audience_score) {
		this.audience_score = audience_score;
	}

	public Bitmap getCover() {
		return cover;
	}

	public void setCover(Bitmap cover) {
		this.cover = cover;
	}

}

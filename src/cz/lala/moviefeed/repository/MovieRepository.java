package cz.lala.moviefeed.repository;

import java.util.ArrayList;

import android.view.View;
import cz.lala.moviefeed.models.Movie;

public class MovieRepository {
	private static ArrayList<Movie> arrayFilms;
	private static int pageNumber = 1;
	private static int pageLimit = 5;
	private static int moviesRamaining;
	private static View mFooter;

	public static ArrayList<Movie> getArrayMovies() {
		return arrayFilms;
	}

	public static void setArrayFilms(ArrayList<Movie> arrayFilms) {
		if (MovieRepository.arrayFilms==null) {
			MovieRepository.arrayFilms = arrayFilms;
		}
		else
		{
			addArrayFilms(arrayFilms);
		}
		
	}
	public static void DeleteAll()
	{
		arrayFilms=null;
		pageNumber = 1;
		pageLimit = 5;
		moviesRamaining=0;
		mFooter=null;
	}
	public static Movie getFilmAtPosition(int position)
	{
		return MovieRepository.arrayFilms.get(position);
	}

	private static void addArrayFilms(ArrayList<Movie> arrayFilms) {
		MovieRepository.arrayFilms.addAll(MovieRepository.arrayFilms.size(), arrayFilms);
	}

	public static int getPageNumber() {
		return pageNumber;
	}

	public static void setPageNumber(int pageNumber) {
		MovieRepository.pageNumber = pageNumber;
	}

	public static int getMoviesRamaining() {
		return moviesRamaining;
	}

	public static void setMoviesRamaining(int moviesDonloaded) {
		MovieRepository.moviesRamaining = moviesDonloaded;
	}
	public static void removeAllMovies()
	{
		MovieRepository.arrayFilms.removeAll(MovieRepository.arrayFilms);
	}
	public static void removeNumberOfDownloadedMovies(int moviesDonloaded)
	{
		MovieRepository.moviesRamaining -= moviesDonloaded;
	}

	public static int getPageLimit() {
		if (pageLimit==moviesRamaining) {
			return (pageLimit+1);
		}
		return pageLimit;
	}

	public static void setPageLimit(int pageLimit) {
		MovieRepository.pageLimit = pageLimit;
	}

	public static View getmFooter() {
		return mFooter;
	}

	public static void setmFooter(View mFooter) {
		MovieRepository.mFooter = mFooter;
	}

}

package cz.lala.moviefeed;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.app.SherlockFragmentActivity;

import cz.lala.moviefeed.R;
import cz.lala.moviefeed.interfaces.OnMovieSelectListener;



public class MainAct extends SherlockFragmentActivity implements OnMovieSelectListener{
	
	Fr_MovieDescsiption fr_MovieDescsiption;
	Fr_MoviesList fr_MoviesList;
	
	private static final String BACKSTAGDESCRIPTION = "Fr_MovieDescsiption";
	private static final String BACKSTAGEFRAGMENT = "Fr_List_To_Fr_Desc";
	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		setContentView(R.layout.act_main);	
		fr_MovieDescsiption = (Fr_MovieDescsiption)getSupportFragmentManager().findFragmentById(R.id.details_frag);
		if (fr_MovieDescsiption==null&&savedInstance==null) {
			getSupportFragmentManager().beginTransaction().add(R.id.act_main_layout,new Fr_MoviesList()).commit();
		}
		
		
	}

	@Override
	public void OnMovieSelect(int position) {
		
		if (fr_MovieDescsiption==null) {
			
			Fr_MovieDescsiption newFragment = new Fr_MovieDescsiption();
            Bundle args = new Bundle();
            args.putInt(Fr_MovieDescsiption.ARG_POSITION, position);
            newFragment.setArguments(args);
        
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.setCustomAnimations(R.anim.come_in2,R.anim.come_in,R.anim.come_out,R.anim.come_out2);
            transaction.replace(R.id.act_main_layout, newFragment,BACKSTAGDESCRIPTION);
            transaction.addToBackStack(BACKSTAGEFRAGMENT);
            // Commit the transaction
            transaction.commit();
		}
		else
		{
			fr_MovieDescsiption.updateMovieDescription(position);
		}
		
	}
	@Override
	public void onBackPressed() {
		 if (getSupportFragmentManager().findFragmentByTag(BACKSTAGDESCRIPTION) != null) {
			   getSupportFragmentManager().popBackStack(BACKSTAGEFRAGMENT,
		       FragmentManager.POP_BACK_STACK_INCLUSIVE);
		   }
		 else{
		super.onBackPressed();
		 }
	}
	

}

package cz.lala.moviefeed;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.MenuItem;

import cz.lala.moviefeed.models.Movie;
import cz.lala.moviefeed.repository.MovieRepository;

public class Fr_MovieDescsiption extends SherlockFragment{
	public static final String ARG_POSITION ="POSITION";

	private  boolean viewsHaveBeenDestroyed = false;
	
	private int mPosition;
	private boolean isMovieSelected;
	private View rootView;
	private TextView mNoMovie;
	private TextView mLenght;
	private TextView mYear;
	private TextView mAuScore;	
	private TextView mCrScore;
	private TextView mName;
	private TextView mDesc;
	private ImageView mCover;
	private TextView mTheatre;
	private TextView mDvd;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setRetainInstance(true);
			}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container, Bundle savedInstanceState) {
		rootView = (ScrollView)inflater.inflate(R.layout.fr_moviedestription, container,false);
		mNoMovie = (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_nomovie);
		mYear= (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_year);
		mDesc = (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_description);
		mName = (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_name);
		mCrScore = (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_rating);
		mAuScore = (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_score);
		mLenght = (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_length);
		mTheatre = (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_theatre);
		mDvd = (TextView)rootView.findViewById(R.id.fr_moviedescription_textview_dvd);
		mCover = (ImageView)rootView.findViewById(R.id.fr_moviedescription_imageview_cover);
		return rootView;
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (null==getArguments()) {
			mNoMovie.setVisibility(View.VISIBLE);
			if (isMovieSelected) {
				showMovieDestription(mPosition);
			}
		}
		else
		{
			getSherlockActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			mPosition=getArguments().getInt(ARG_POSITION);
			showMovieDestription(mPosition);
		}
	}
	
	public void updateMovieDescription(int position)
	{
		mPosition=position;
		showMovieDestription(position);
	}
	private void showMovieDestription(int position)
	{
		isMovieSelected=true;
		if (mNoMovie.getVisibility()==View.VISIBLE) {
			mNoMovie.setVisibility(View.GONE);
		}
		Movie mSelectedMovie = MovieRepository.getArrayMovies().get(position);
		 mLenght.setText(getResources().getString(R.string.fr_moviedescription_length)+" "+String.valueOf(mSelectedMovie.getLength()) +" mins");
		 mYear.setText(getResources().getString(R.string.fr_moviedescription_year)+" "+String.valueOf(mSelectedMovie.getYear()));
		 mAuScore.setText(getResources().getString(R.string.fr_moviedescription_au_score)+" "+ (mSelectedMovie.getAudience_score() != (-1) ? mSelectedMovie.getAudience_score()+"%" : getResources().getString(R.string.fr_moviedescription_unknow)));
		 mCrScore.setText(getResources().getString(R.string.fr_moviedescription_cr_score)+" "+ (mSelectedMovie.getCritic_score() != (-1) ? mSelectedMovie.getCritic_score()+"%" : getResources().getString(R.string.fr_moviedescription_unknow)));
		 mName.setText(mSelectedMovie.getName());
		 mDesc.setText(getResources().getString(R.string.fr_moviedescription_desc)+"\n"+ mSelectedMovie.getDescribe());
		 mTheatre.setText(getResources().getString(R.string.fr_moviedescription_theater)+" "+String.valueOf(mSelectedMovie.getDateTheater()));
		 mDvd.setText(getResources().getString(R.string.fr_moviedescription_dvd)+" "+ (mSelectedMovie.getDateDvd() != null ? mSelectedMovie.getDateDvd() : getResources().getString(R.string.fr_moviedescription_unknow)));
		 mCover.setImageBitmap(mSelectedMovie.getCover());
		
		
	}
	
	// Stop Anim on Rotate
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		 case android.R.id.home:
		        getSherlockActivity().onBackPressed();
		        return true;
		    }
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
	    // This stops animation on rotation as we have a retained instance.
	    boolean shouldNotAnimate = enter && viewsHaveBeenDestroyed;
	    viewsHaveBeenDestroyed = false;
	    return shouldNotAnimate ? AnimationUtils.loadAnimation(getActivity(), R.anim.none)
	            : super.onCreateAnimation(transit, enter, nextAnim);
	}

	@Override
	public void onDestroyView() {
	    super.onDestroyView();
	    viewsHaveBeenDestroyed = true;
	}

	

}
